import {Movie} from "./movie";
import {Rental} from "./rental";

export class Customer {

    private name: string;
    private rentals: Rental[] = [];

    public constructor(name: string) {
        this.name = name;
    }

    public addRental(arg: Rental) {
        this.rentals.push(arg);
    }

    public getName(): string {
        return this.name;
    }

    public statement(): string {
        let totalAmount: number = 0;
        let frequentRenterPoints: number = 0;
        let result = "Rental Record for " + this.getName() + "\n";

        for (const rental of this.rentals) {
            let rentalAmount = 0;

            // determine amounts for each line
            rentalAmount = rental.getAmount();

            // add frequent renter points
            frequentRenterPoints += rental.getFrequentRenterPoints();

            // show figures for this rental
            result += "\t" + rental.getMovieTitle() + "\t" + rentalAmount.toFixed(1) + "\n";
            totalAmount += rentalAmount;
        }

        // add footer lines
        result += "Amount owed is " + totalAmount.toFixed(1) + "\n";
        result += "You earned " + frequentRenterPoints + " frequent renter points";

        return result;
    }



// <h1>Rental Record for <em>martin</em></h1>
// <table>
// <tr><td>Ran</td><td>3.5</td></tr>
// <tr><td>Trois Couleurs: Bleu</td><td>2</td></tr>
// </table>
// <p>Amount owed is <em>5.5</em></p>
// <p>You earned <em>2</em> frequent renter points</p>
}