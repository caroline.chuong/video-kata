import {Movie} from "./movie";

export class Rental {

    private movie: Movie;
    private daysRented: number;
    public static PENALITY_FACTOR = 1.5;
    public static INCLUDED_REGULAR_RENTAL_DAYS = 2;
    public static INCLUDED_CHILDRENS_RENTAL_DAYS = 3;

    public constructor(movie: Movie, daysRented: number) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public getDaysRented(): number {
        return this.daysRented;
    }

    public getMovie(): Movie {
        return this.movie;
    }

    public getMoviePriceCode(){
        return this.movie.getPriceCode()
    }

    public getMovieTitle(){
        return this.movie.getTitle()
    }

    public getAmount(): number {
        let amount = 0
        switch (this.getMoviePriceCode()) {
            case Movie.REGULAR:
                amount += Movie.REGULAR_DAILY_PRICE;
                amount += this.applyPenality(Rental.INCLUDED_REGULAR_RENTAL_DAYS);
                break;
            case Movie.NEW_RELEASE:
                amount += this.getDaysRented() * Movie.NEW_RELEASE_DAILY_PRICE;
                break;
            case Movie.CHILDRENS:
                amount += Movie.CHILDRENS_DAILY_PRICE;
                amount += this.applyPenality(Rental.INCLUDED_CHILDRENS_RENTAL_DAYS)
                break;
        }
        return amount;
    }

    private applyPenality(includedRentalDays: number): number {
        if (this.getDaysRented() > includedRentalDays) {
            return (this.getDaysRented() - includedRentalDays) * Rental.PENALITY_FACTOR;
        }
        return 0
    }

    public getFrequentRenterPoints(): number {
        // add bonus for a two day new release rental
        if ((this.getMoviePriceCode() === Movie.NEW_RELEASE) && this.getDaysRented() > 1)
            return 2
        return 1
    }
}